How to start using Cloudwords for Drupal 7 today...

If you do not already have an existing Cloudwords account, you can signup for
one at http://www.cloudwords.com/editions/start-today/. You will receive a
welcome email with instructions on how to access your new Cloudwords account.

From within your Cloudwords account, click on “Settings” in the upper right
corner, then click on “My Account”, then click on “API”. Or, use this url:
https://app.cloudwords.com/cust.htm#settings/myaccount/api. Follow the
instructions to generate an API key for your user.

Download and install the Cloudwords for Drupal integration module into your
Drupal instance from here: http://drupal.org/project/cloudwords.

From within Drupal click on “Configuration”, then “Web Services”, then
“Cloudwords”.  Copy the API key generated within Cloudwords into the
“API Authorization Token” field.  Click on “Save configuration”.

You should now be ready to start using Cloudwords for Drupal. To begin using,
from within Drupal click on “Structure” and then “Cloudwords”. You can begin
selecting Drupal content that needs translation, submitting the selected content
to Cloudwords, and easily manage the translation of the content via a Cloudwords
project.
